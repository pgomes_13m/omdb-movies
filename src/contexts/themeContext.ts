import React from 'react'

export interface IThemes {
  light: StyleType
  dark: StyleType
}

export type StyleType = {
  bgColor: string
}

export const themes = {
  light: {
    bgColor: 'Azure',
  },
  dark: {
    bgColor: 'Salmon',
  },
}

export const ThemeContext = React.createContext(themes.light)
