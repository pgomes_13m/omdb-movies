import { ACTIONS } from '../types'

export const setSearchTermAction = (searchTerm: string) => ({
  type: ACTIONS.SET_SEARCH_TERM,
  searchTerm,
})
