import { getMoviesAction, getMoviesErrorAction } from './getMoviesAction'
import { addMovieToPlaylistAction } from './addMovieToPlaylistAction'
import { removeMovieFromPlaylistAction } from './removeMovieFromPlaylistAction'
import { requestFetchingAction } from './requestFetchingAction'
import { setMoviesAction } from './setMoviesAction'
import { setSearchTermAction } from './setSearchTermAction'

export {
  getMoviesAction,
  getMoviesErrorAction,
  addMovieToPlaylistAction,
  removeMovieFromPlaylistAction,
  requestFetchingAction,
  setMoviesAction,
  setSearchTermAction,
}
