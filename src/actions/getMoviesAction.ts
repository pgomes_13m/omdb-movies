import { ACTIONS, RequestConfigType } from '../types'

export const getMoviesAction = (config: RequestConfigType) => ({
  type: ACTIONS.GET_MOVIES_REQUEST,
  config,
})

export const getMoviesErrorAction = (error: Error) => ({
  type: ACTIONS.GET_MOVIES_REQUEST_ERROR,
  error,
})
