import { ACTIONS, IMovie } from '../types'

export const addMovieToPlaylistAction = (movie: IMovie) => ({
  type: ACTIONS.ADD_MOVIE_TO_PLAYLIST,
  movie,
})
