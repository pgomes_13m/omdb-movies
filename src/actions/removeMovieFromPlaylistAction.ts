import { ACTIONS } from '../types'

export const removeMovieFromPlaylistAction = (id: string) => ({
  type: ACTIONS.REMOVE_MOVIE_FROM_PLAYLIST,
  id,
})
