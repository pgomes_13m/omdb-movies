import { ACTIONS } from '../types'

export const requestFetchingAction = (isFetching: Boolean) => ({
  type: ACTIONS.REQUEST_FETCHING,
  isFetching,
})
