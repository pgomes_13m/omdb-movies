import { ACTIONS, IMovie } from '../types'

export const setMoviesAction = (movies: IMovie[]) => ({
  type: ACTIONS.SET_MOVIES,
  movies,
})
