import { ACTIONS, IErrorState } from '../types'

const initialState: IErrorState = {
  error: null,
}

function reducer(state = initialState, action: any) {
  switch (action.type) {
    case ACTIONS.GET_MOVIES_REQUEST_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}

export default reducer
