import { combineReducers } from 'redux'

import dataReducer from './dataReducer'
import errorReducer from './errorReducer'
import { IRootState } from '../types'

const rootReducer = combineReducers<IRootState>({
  data: dataReducer as any,
  error: errorReducer as any,
})

export default rootReducer
