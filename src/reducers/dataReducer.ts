import _ from 'lodash'

import { ACTIONS, IDataState } from '../types'

const initialState: IDataState = {
  searchTerm: '',
  movies: [],
  playlist: [],
  isFetching: false,
}

function reducer(state = initialState, action: any) {
  switch (action.type) {
    case ACTIONS.SET_SEARCH_TERM:
      return {
        ...state,
        searchTerm: action.searchTerm,
      }

    case ACTIONS.GET_MOVIES_REQUEST_SUCCESS:
      // remove the duplicate movies from the response
      const uniqMovies = _.uniqBy(action.movies, 'imdbID')
      return {
        ...state,
        movies: uniqMovies,
      }

    case ACTIONS.SET_MOVIES:
      return {
        ...state,
        movies: action.movies,
      }

    case ACTIONS.ADD_MOVIE_TO_PLAYLIST:
      return {
        ...state,
        playlist: [...state.playlist, action.movie],
      }

    case ACTIONS.REMOVE_MOVIE_FROM_PLAYLIST:
      return {
        ...state,
        playlist: state.playlist.filter(movie => movie.imdbID !== action.id),
      }

    case ACTIONS.REQUEST_FETCHING:
      return {
        ...state,
        isFetching: action.isFetching,
      }

    default:
      return state
  }
}

export default reducer
