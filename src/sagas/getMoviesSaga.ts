import { put, call } from 'redux-saga/effects'

import { ACTIONS, RequestConfigType, ISearchData } from '../types'
import { request } from '../utils'

const getMoviesData = async (config: RequestConfigType) => {
  return request(config)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err)
    })
}

/**
 * Responsible for making the api calls to the omdb api,
 * and instructing the redux-saga middle ware on the next line of action,
 * for success or failure operation.
 */
export function* getMoviesSaga({ config }: { config: RequestConfigType }) {
  yield put({ type: ACTIONS.REQUEST_FETCHING, isFetching: true })
  try {
    const data: ISearchData = yield call(getMoviesData, config)
    const { Response = 'False', Search } = data
    const movies = Response === 'True' ? Search : []

    yield put({ type: ACTIONS.GET_MOVIES_REQUEST_SUCCESS, movies })
  } catch (error) {
    yield put({ type: ACTIONS.GET_MOVIES_REQUEST_ERROR, error })
  } finally {
    yield put({ type: ACTIONS.REQUEST_FETCHING, isFetching: false })
  }
}
