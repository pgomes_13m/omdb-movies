import { takeLatest } from 'redux-saga/effects'

import { ACTIONS } from '../types'
import { getMoviesSaga } from './getMoviesSaga'

export default function* watch() {
  yield takeLatest(ACTIONS.GET_MOVIES_REQUEST, getMoviesSaga)
}
