import { FC } from 'react'

export type RouteType = {
  path: string
  component?: FC
  redirect?: boolean
  to?: string
}

export type IRoutes = Array<RouteType>
