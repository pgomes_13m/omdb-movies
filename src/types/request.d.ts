import { HttpMethod } from './enums'

export type HttpMethodStringType = keyof typeof HttpMethod

export type RequestConfigType = {
  baseUrl?: string
  headers?: object
  params: ParamsType
  payload?: object
  method?: HttpMethodStringType
}

export interface IRequest {
  getRequestOptions(): void
  getResourceUrl(): String
  promise(): Promise<Object>
}

export type ParamsType = {
  [key: string]: string | number
}

export type HeadersType = {
  [key: string]: string | number
}
