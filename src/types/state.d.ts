import { IMovie } from './movie'

export interface IRootState {
  data: IDataState
  error: IErrorState
}

export interface IDataState {
  movies: Array<IMovie>
  playlist: Array<IMovie>
  searchTerm: string
  isFetching: Boolean
}

export interface IErrorState {
  error: Error | null
}
