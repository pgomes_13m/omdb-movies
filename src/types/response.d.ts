export interface ISearchData {
  Response: string
  Search: Array<object>
  totalResults?: number
  Error?: string
}
