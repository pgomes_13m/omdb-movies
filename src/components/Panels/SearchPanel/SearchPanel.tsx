import { ChangeEvent, Fragment, FunctionComponent } from 'react'
import { Typography, Box, TextField, Fab } from '@mui/material'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'

export interface ISearchPanelProps {
  searchTerm: string
  title: string
  description: string
  handleSearch: (e: ChangeEvent<HTMLInputElement>) => void
  handleClear: () => void
}

const SearchPanel: FunctionComponent<ISearchPanelProps> = ({
  searchTerm,
  title,
  description,
  handleSearch,
  handleClear,
}) => (
  <Fragment>
    <Box
      sx={{
        pt: 8,
        pb: 6,
      }}
    >
      <Typography component="h1" variant="h2" align="center" color="text.primary" gutterBottom>
        {title}
      </Typography>
      <Typography variant="h5" align="center" color="text.secondary" paragraph>
        {description}
      </Typography>
      <TextField fullWidth label="Search..." variant="outlined" value={searchTerm} onChange={handleSearch} />
      {!!searchTerm.length && (
        <Fab variant="extended" size="small" color="primary" aria-label="add" sx={{ mt: 1 }} onClick={handleClear}>
          <HighlightOffIcon sx={{ mr: 1 }} />
          Clear
        </Fab>
      )}
    </Box>
  </Fragment>
)

export { SearchPanel }
