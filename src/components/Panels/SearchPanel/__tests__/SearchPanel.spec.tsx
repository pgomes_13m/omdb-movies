import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'
import { Typography, Button } from '@mui/material'
import { SearchPanel } from '../SearchPanel'

Enzyme.configure({ adapter: new Adapter() })

describe('Header', () => {
  const props = {
    title: 'OMDb Movies',
    description: 'Enjoy your movies',
    handleSearch: jest.fn,
  }
  it('renders correctly', () => {
    const wrapper = shallow(<SearchPanel {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should display the title', () => {
    const wrapper = shallow(<SearchPanel {...props} />)
    expect(wrapper.find(Typography).first().text()).toEqual(props.title)
  })

  it('should display the description', () => {
    const wrapper = shallow(<SearchPanel {...props} />)
    expect(wrapper.find(Typography).at(1).text()).toEqual(props.description)
  })
})
