import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'
import { Album } from '../../../index'
import { ResultsPanel } from '../ResultsPanel'

Enzyme.configure({ adapter: new Adapter() })

describe('ResultsPanel', () => {
  const props = {
    movies: [
      {
        Title: 'Harry Potter',
        Year: '2008',
        Type: 'movie',
        Poster: 'http://www.test.com/img1',
        imdbID: '4321',
      },
      {
        Title: 'Harry Potter 2',
        Year: '2010',
        Type: 'movie',
        Poster: 'http://www.test.com/img2',
        imdbID: '4322',
      },
    ],
  }
  it('renders correctly', () => {
    const wrapper = shallow(<ResultsPanel {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('renders the albums as per the movies passed', () => {
    const wrapper = shallow(<ResultsPanel {...props} />)
    expect(wrapper.find(Album).length).toEqual(2)
  })
})
