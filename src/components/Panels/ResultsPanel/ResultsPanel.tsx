import React, { Fragment, FunctionComponent } from 'react'
import { Grid } from '@mui/material'

import { Album } from '../../index'
import { IMovie } from '../../../types'

export interface IResultsPanelProps {
  movies: IMovie[]
  playlistMovieIds?: string[]
  addMovieToPlaylist?: (movie: IMovie) => void
  removeMovieFromPlaylist?: (id: string) => void
}

const ResultsPanel: FunctionComponent<IResultsPanelProps> = ({
  movies,
  addMovieToPlaylist,
  playlistMovieIds,
  removeMovieFromPlaylist,
}) => (
  <Fragment>
    <Grid container spacing={4}>
      {movies.map(movie => (
        <Album
          Title={movie.Title}
          key={movie.imdbID}
          Year={movie.Year}
          imdbID={movie.imdbID}
          Type={movie.Type}
          Poster={movie.Poster}
          addMovieToPlaylist={addMovieToPlaylist}
          removeMovieFromPlaylist={removeMovieFromPlaylist}
          playlistMovieIds={playlistMovieIds}
        />
      ))}
    </Grid>
  </Fragment>
)

export { ResultsPanel }
