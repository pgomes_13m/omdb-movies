import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'
import { Typography, Button } from '@mui/material'
import { Album } from '../Album'

Enzyme.configure({ adapter: new Adapter() })

describe('Album', () => {
  const props = {
    Title: 'Harry Potter',
    Year: '2008',
    Type: 'movie',
    Poster: 'http://www.test.com/img',
    imdbID: '4321',
    addMovieToPlaylist: jest.fn,
    removeMovieFromPlaylist: jest.fn,
    playlistMovieIds: ['1234'],
  }
  it('renders correctly', () => {
    const wrapper = shallow(<Album {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should display the title', () => {
    const wrapper = shallow(<Album {...props} />)
    expect(wrapper.find(Typography).first().text()).toEqual(props.Title)
  })

  it('should display the year and type', () => {
    const wrapper = shallow(<Album {...props} />)
    expect(wrapper.find(Typography).at(1).text()).toEqual(`${props.Year} ${props.Type}`)
  })

  it('should display "Add to Playlist" button', () => {
    const wrapper = shallow(<Album {...props} />)
    expect(wrapper.find(Button).text()).toEqual('Add to Playlist')
  })
})
