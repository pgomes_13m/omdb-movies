import { FunctionComponent } from 'react'
import { Card, CardMedia, CardContent, Typography, CardActions, Button, Grid } from '@mui/material'

import { IMovie } from '../../types'
export interface IAlbumProps extends IMovie {
  addMovieToPlaylist?: (movie: IMovie) => void
  removeMovieFromPlaylist?: (id: string) => void
  playlistMovieIds?: Array<string>
}

const Album: FunctionComponent<IAlbumProps> = ({
  Title,
  Year,
  Type,
  Poster,
  imdbID,
  addMovieToPlaylist,
  removeMovieFromPlaylist,
  playlistMovieIds,
}) => (
  <Grid item key={imdbID} xs={12} sm={6} md={4}>
    <Card sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
      <CardMedia
        component="img"
        image={Poster === 'N/A' ? 'https://via.placeholder.com/450?text=No+Image+Found' : Poster}
        alt={Title}
      />
      <CardContent sx={{ flexGrow: 1 }}>
        <Typography gutterBottom variant="h5" component="h2">
          {Title}
        </Typography>
        <Typography gutterBottom variant="h6" component="h4">
          {Year} {Type}
        </Typography>
      </CardContent>
      <CardActions>
        {playlistMovieIds && playlistMovieIds.find(id => id === imdbID) ? (
          <Button
            variant="contained"
            color="error"
            size="small"
            onClick={() => removeMovieFromPlaylist && removeMovieFromPlaylist(imdbID)}
          >
            Remove from Playlist
          </Button>
        ) : (
          <Button
            variant="contained"
            size="small"
            onClick={() => addMovieToPlaylist && addMovieToPlaylist({ Title, Year, Type, Poster, imdbID })}
          >
            Add to Playlist
          </Button>
        )}
      </CardActions>
    </Card>
  </Grid>
)

export { Album }
