import { FunctionComponent } from 'react'
import { Typography, Box } from '@mui/material'
import './Footer.css'

export interface IFooterProps {
  favColor?: string
}

/**
 * The Footer component consisting favourite color
 */
const Footer: FunctionComponent<IFooterProps> = ({ favColor }) => (
  <Box sx={{ p: 2 }} component="footer" className="footer">
    <Typography variant="h6" align="center" gutterBottom>
      OMDb Movies
    </Typography>
    <Typography variant="subtitle1" component="p">
      {favColor && `My favourite color is ${favColor}`}
    </Typography>
  </Box>
)

export { Footer }
