import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'
import { Typography } from '@mui/material'
import { Footer } from '../Footer'

Enzyme.configure({ adapter: new Adapter() })

describe('Footer', () => {
  const props = {
    favColor: 'Azure',
  }
  it('renders correctly', () => {
    const wrapper = shallow(<Footer {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should display favourite color text', () => {
    const wrapper = shallow(<Footer {...props} />)
    expect(wrapper.find(Typography).at(1).text()).toEqual('My favourite color is Azure')
  })
})
