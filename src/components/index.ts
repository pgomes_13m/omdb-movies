import { Header, IHeaderProps } from './Header/Header'
import { Footer } from './Footer/Footer'
import { SearchPanel, ISearchPanelProps } from './Panels/SearchPanel/SearchPanel'
import { ResultsPanel } from './Panels/ResultsPanel/ResultsPanel'
import { Album } from './Album/Album'

// export components
export { Header, Footer, SearchPanel, ResultsPanel, Album }

// export types
export type { IHeaderProps, ISearchPanelProps }
