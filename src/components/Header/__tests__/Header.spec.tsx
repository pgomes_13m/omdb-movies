import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'
import { Typography, Button } from '@mui/material'
import { Header } from '../Header'
import { RESOURCES } from '../../../types'

Enzyme.configure({ adapter: new Adapter() })

describe('Header', () => {
  const props = {
    title: 'OMDb Movies',
    resource: RESOURCES.PLAYLIST,
  }
  it('renders correctly', () => {
    const wrapper = shallow(<Header {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should display the title', () => {
    const wrapper = shallow(<Header {...props} />)
    expect(wrapper.find(Typography).text()).toEqual(props.title)
  })

  it('should display the correct resource link', () => {
    const wrapper = shallow(<Header {...props} />)
    expect(wrapper.find(Button).text()).toEqual(props.resource)
  })
})
