import { FunctionComponent } from 'react'
import { useHistory } from 'react-router-dom'
import { AppBar, Toolbar, Typography, Box, Button } from '@mui/material'
import CameraIcon from '@mui/icons-material/PhotoCamera'
import { RESOURCES } from '../../types'

export interface IHeaderProps {
  title: string
  resource: RESOURCES
}

/**
 * The Header component consisting of the header info
 */
const Header: FunctionComponent<IHeaderProps> = ({ title, resource }) => {
  const history = useHistory()

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed">
        <Toolbar>
          <CameraIcon sx={{ mr: 2 }} />
          <Typography variant="h6" color="inherit" sx={{ flexGrow: 1 }}>
            {title}
          </Typography>
          <Button color="inherit" onClick={() => history.push(`/${resource}`)}>
            {resource}
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export { Header }
