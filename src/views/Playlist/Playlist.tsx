import { Fragment, FunctionComponent, useContext } from 'react'
import { Container, CssBaseline, Typography, Box } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'

import { Header, ResultsPanel, Footer } from '../../components'
import { playlistSelector } from '../../selectors'
import { IMovie, RESOURCES } from '../../types'
import { addMovieToPlaylistAction, removeMovieFromPlaylistAction } from '../../actions'
import { ThemeContext } from '../../contexts'
import './Playlist.css'

const Playlist: FunctionComponent = () => {
  const playlistTitle = 'Your Playlist!'
  let playlistDescription = ''
  const dispatch = useDispatch()
  const theme = useContext(ThemeContext)
  const favColor = theme.bgColor

  // selectors
  const playlist = useSelector(playlistSelector)
  playlistDescription = playlist.length
    ? 'Your favourite IMDb movies now at one place.'
    : 'Your playlist is currently empty.'

  //methods
  const handleAddMovieToPlaylist = (movie: IMovie) => {
    dispatch(addMovieToPlaylistAction(movie))
  }

  const handleRemoveMovieFromPlaylist = (id: string) => {
    dispatch(removeMovieFromPlaylistAction(id))
  }

  return (
    <Fragment>
      <Fragment>
        <CssBaseline />
        <Header title="OMDb Playlist" resource={RESOURCES.HOME} />
        <Container sx={{ py: 8 }} maxWidth="md" className="playlist-content">
          <Box
            sx={{
              pt: 8,
              pb: 6,
            }}
          >
            <Typography component="h1" variant="h2" align="center" color="text.primary" gutterBottom>
              {playlistTitle}
            </Typography>
            <Typography variant="h5" align="center" color="text.secondary" paragraph>
              {playlistDescription}
            </Typography>
          </Box>

          <ResultsPanel
            movies={playlist}
            playlistMovieIds={playlist.map((movie: IMovie) => movie.imdbID)}
            addMovieToPlaylist={handleAddMovieToPlaylist}
            removeMovieFromPlaylist={handleRemoveMovieFromPlaylist}
          />
        </Container>
      </Fragment>
      <Footer favColor={favColor} />
    </Fragment>
  )
}

export default Playlist
