import { ChangeEvent, Fragment, useEffect, FunctionComponent, useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CssBaseline from '@mui/material/CssBaseline'
import { Container, Skeleton, Fab, Box, Typography, Alert, AlertTitle } from '@mui/material'
import { useHistory } from 'react-router-dom'

import { Header, SearchPanel, ResultsPanel, Footer } from '../../components'
import {
  getMoviesAction,
  addMovieToPlaylistAction,
  removeMovieFromPlaylistAction,
  setSearchTermAction,
  setMoviesAction,
} from '../../actions'
import { RequestConfigType, IMovie, RESOURCES } from '../../types'
import {
  moviesSelector,
  searchTermSelector,
  playlistSelector,
  isFetchingSelector,
  errorSelector,
} from '../../selectors'
import { ThemeContext } from '../../contexts'
import './Home.css'

const Home: FunctionComponent = () => {
  const searchTitle = 'Favourite OMDB movies!'
  const searchDescription = 'Start typing your movie name in the search box below.'
  const dispatch = useDispatch()
  const history = useHistory()
  const theme = useContext(ThemeContext)

  // selectors
  const searchTerm = useSelector(searchTermSelector)
  const movies = useSelector(moviesSelector)
  const playlist = useSelector(playlistSelector)
  const isLoading = useSelector(isFetchingSelector)
  const error: Error | null = useSelector(errorSelector)

  // get movies by search term
  useEffect(() => {
    const config: RequestConfigType = {
      params: {
        s: searchTerm,
      },
    }
    if (searchTerm.length) dispatch(getMoviesAction(config))
  }, [dispatch, searchTerm])

  // methods
  const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
    const term = event.target.value || ''
    dispatch(setSearchTermAction(term))
  }

  const handleClear = () => {
    dispatch(setSearchTermAction(''))
    dispatch(setMoviesAction([]))
  }

  const handleAddMovieToPlaylist = (movie: IMovie) => {
    dispatch(addMovieToPlaylistAction(movie))
  }

  const handleRemoveMovieFromPlaylist = (id: string) => {
    dispatch(removeMovieFromPlaylistAction(id))
  }

  const getPlaylistMovieIds = () => {
    if (!playlist.length) {
      return []
    }

    return playlist.map((movie: IMovie) => movie.imdbID)
  }

  // render methods
  const renderConfirmPlaylist = () => (
    <Box display="flex" justifyContent="center" alignItems="center" sx={{ mb: 5 }}>
      <Fab variant="extended" color="primary" aria-label="add" onClick={() => history.push('/playlist')}>
        Confirm Playlist
      </Fab>
    </Box>
  )

  const renderNoMoviesFound = () => (
    <Typography variant="h5" align="center" color="text.secondary" paragraph>
      No movies found! Please try again.
    </Typography>
  )

  const renderErrorAlert = () => (
    <Alert severity="error" sx={{ mt: 5 }}>
      <AlertTitle>API Error!</AlertTitle>
      {error && error.message ? error.message : 'Please check the console for more details.'}
    </Alert>
  )

  return (
    <Fragment>
      <CssBaseline />
      <Header title="OMDb Movies" resource={RESOURCES.PLAYLIST} />
      <Container sx={{ py: 8, bgcolor: theme.bgColor }} maxWidth="md" className="home-content">
        <SearchPanel
          title={searchTitle}
          description={searchDescription}
          searchTerm={searchTerm}
          handleSearch={handleSearch}
          handleClear={handleClear}
        />
        {!!playlist.length && renderConfirmPlaylist()}
        {!movies.length && !!searchTerm.length && renderNoMoviesFound()}
        {isLoading ? (
          <Skeleton variant="rectangular" width={210} height={118} />
        ) : (
          <ResultsPanel
            movies={movies}
            playlistMovieIds={getPlaylistMovieIds()}
            addMovieToPlaylist={handleAddMovieToPlaylist}
            removeMovieFromPlaylist={handleRemoveMovieFromPlaylist}
          />
        )}
        {!!error && renderErrorAlert()}
      </Container>
      <Footer />
    </Fragment>
  )
}

export default Home
