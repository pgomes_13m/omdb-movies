import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet'
import { Provider } from 'react-redux'
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom'
import { createTheme, ThemeProvider } from '@mui/material/styles'

import { configureStore } from './store'
import { ThemeContext, themes } from './contexts'
import Routes from './routes'
import './index.css'

const publicPath = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_PROD_PUBLIC_PATH : '/'
const favouriteTheme = themes.light
const theme = createTheme({
  palette: {
    background: {
      default: favouriteTheme.bgColor,
    },
  },
})

ReactDOM.render(
  <Provider store={configureStore()}>
    <ThemeProvider theme={theme}>
      <ThemeContext.Provider value={themes.light}>
        <Helmet>
          <title>OBDb Movies</title>
          <meta name="description" content="OMDb Movies" />
        </Helmet>
        <BrowserRouter basename={publicPath}>
          <Switch>
            {Routes.map((route, key) => {
              if (route.redirect) {
                return <Redirect exact from={route.path} to={route.to} key={key} />
              }
              return <Route exact path={route.path} component={route.component} key={key} />
            })}
          </Switch>
        </BrowserRouter>
      </ThemeContext.Provider>
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
)
