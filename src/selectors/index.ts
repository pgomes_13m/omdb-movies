import { IRootState } from '../types'

/**
 * Get the movies array from the store
 */
export const moviesSelector = (state: IRootState) => {
  return state.data.movies || []
}

/**
 * Get the search term
 */
export const searchTermSelector = (state: IRootState) => {
  return state.data.searchTerm || ''
}

/**
 * Get the playlist array from the store
 */
export const playlistSelector = (state: IRootState) => {
  return state.data.playlist || []
}

/**
 * Get the request fetching status from the store
 */
export const isFetchingSelector = (state: IRootState) => {
  return state.data.isFetching || false
}

/**
 * Get the error from the store
 */
export const errorSelector = (state: IRootState) => {
  return state.error.error || null
}
