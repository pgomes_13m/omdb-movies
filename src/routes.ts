import { Home, Playlist } from './views'

const Routes = [
  {
    path: '/home',
    component: Home,
  },
  {
    path: '/playlist',
    component: Playlist,
  },
  {
    redirect: true,
    path: '/',
    to: '/home',
  },
]

export default Routes
