import { Request } from '../request'

describe('Request', () => {
  let instance: Request

  beforeEach(() => {
    instance = new Request('GET', { s: 'harry' })
    instance.baseUrl = 'http://www.test.com'
    instance.apiKey = 'xxxx'
  })

  it('should build correct resource url', () => {
    const resourceUrl = instance.getResourceUrl()
    expect(resourceUrl).toEqual('http://www.test.com?s=harry&&apikey=xxxx')
  })

  it('should get the correct request options', () => {
    const requestOptions = instance.getRequestOptions()
    const expected = {
      method: 'GET',
      url: 'http://www.test.com?s=harry&&apikey=xxxx',
    }
    expect(requestOptions).toEqual(expected)
  })
})
