# Omdb Movies

React App to display movies using the OMDb API - https://www.omdbapi.com

## Demo
https://pgomes_13m.gitlab.io/omdb-movies/

## Synopsis

The app makes use of following open source projects -

* [React] - A JavaScript library for building user interfaces
* [Typescript] - A strongly typed programming language that builds on Javascript.
* [Redux] - Redux is a predictable state container for JavaScript apps
* [redux-saga] - redux-saga is a library that aims to make application side effects easier to manage
* [Webpack] - bundle your assets
* [Mui] - Material UI consists of React components that implement Google's Material Design
* [Jest] - Delightful JavaScript Testing

## Installation

Clone this repository on your local machine. Run the following commands in the root directory - 

```sh
npm install
```

### Development
```sh
npm run start
```

### Production
```sh
npm run build
```

### Testing
```sh
npm test
```

## Contributors
[Pascoal Gomes](https://au.linkedin.com/in/pascoal-gomes-a4835954)

[React]: <https://reactjs.org/>
[Typescript]: <https://www.typescriptlang.org/>
[Redux]: <https://redux.js.org/>
[redux-saga]: <https://redux-saga.js.org/>
[Webpack]: <https://webpack.js.org//>
[Mui]: <https://mui.com/>
[Jest]: <https://jestjs.io/>
